console.log('index');

/* server location */
let socket = io('http://105.78.234.75:3000');

$(document).ready(function() {

/* handler for header__buttons */
	$('#header__btns').on('click', 'button', function() {
		console.log($(this).val());
		/* emit a refresh process */

		if ($(this).val() === 'test') {
			showMessage('test');
		}

		socket.emit('pushedButton', {
			button: $(this).val()
		});
	});

/* handler for switch gpio pin */
	$('#pinUL').on('click', 'span', function() {
		/* debug
		console.log('parent id: ', $(this).parent().attr('id'));
		console.log('text: ', $(this).text()); */

		if ($(this).text() === 'Off') {
			let pinClicked = $(this);

			/* Sets the pushed button to Loading to prevent multiple events
			$(this).toggleClass('loading').text('Loading'); */

		/* gpio button now switched visually to on */
			$(this)
				.text('On')
				.toggleClass('on')
				.toggleClass('off');

		/* send the pins wich needs to set on */
			socket.emit('switchPin', {
				pin: ($(this).parent().attr('id')).match(/\d{2}$/),
				stat: $(this).text()
			});

			/* setTimeout for testing purpose
			setTimeout(function() {
				if (pinClicked.hasClass('loading')) {
					console.log('test');
					pinClicked
						.text('On')
						.toggleClass('off')
						.toggleClass('on');
				}
			}, 1000); */

		} else if ($(this).text() === 'On') {

			/* Sets the pushed button to Loading to prevent multiple events
			$(this).toggleClass('loading').text('Loading'); */

		/* gpio button now switched visually to off */
			$(this)
				.text('Off')
				.toggleClass('off')
				.toggleClass('on');

		/* send the pins wich needs to set off */
			socket.emit('switchPin', {
				pin: ($(this).parent().attr('id')).match(/\d{2}$/),
				stat: $(this).text()
			});
		}
	});

/* handler accordion */
	$('#accordion__button__gpio').on('click', function() {
		/*
		console.log('click on accordion button gpio'); */

		$('#accordion__panel__gpio').slideToggle(1000);
	});

/* connect event */
	socket.on('connect', function() {
		console.log('connected to server');
	});

/* receive data event */
	socket.on('sendStat', function(data) {
		// console.log('receiving data from server');
		// console.log('data:', data);

		updateInfos(data);
	});

/* receive response event */
	socket.on('sendResponsMessage', function(message) {
		console.log('receiving message from server');
		console.log('message:', message);

		showMessage(message);
	});

/* receive countdown response event */
	socket.on('countdownMessage', function(message) {
		console.log('receiving countdown message from server');
		console.log('message:', message);
		message.countdown = 5;

		showMessage(message);

	/* exit node app */
		let countdown = 5;
		let countdownInterval = setInterval(function() {
			console.log(countdown);

			console.log(message.message + countdown);
			message.countdown = countdown;

			showMessage(message);
			countdown--;

			if (countdown === 0) {
				clearInterval(countdownInterval);
				console.log('Raspberry is restarting now');
				showMessage('Raspberry is restarting now');
			}
		}, 1000);

	});

/* disconnect event */
	socket.on('disconnect', function() {
		console.log('disconnected from server');
	});





/* Update Infos */
	function updateInfos(update) {
		$('#info__users').text('hallo');
		console.log(update);

		$('#info__users').text(update.users);
		$('#info__uptime').text(update.uptime);
		$('#info__cpuTemp').text(update.cpuTemp);
	}

/* Show Message */
	function showMessage(message) {
		let snackbar = $('.snackbar');
		snackbar.text(`${message.message} ${message.countdown}`);

		snackbar.animate({
			bottom: '30px',
			opacity: '1'
		}, 'fast');

		setTimeout(function() {
			snackbar.animate({
				bottom: '15px',
				opacity: 0
			}, 'slow');

		}, 3000);
	};

});