console.log('Log: raspberry monitoring');

/* dependencies */
	const path = require('path');
	const http = require('http');
	const express = require('express');
	const socketIO = require('socket.io');
	const exec = require('child_process').exec;

	const Gpio = require('onoff').Gpio;

	const pathToScript = '/home/pi/Desktop/projects/node-raspberry-rbpMonitoring/server/scripts/script_getInfos';
	const pathToRestartRaspberryScript = '/home/pi/Desktop/projects/node-raspberry-rbpMonitoring/server/scripts/script_restartRaspberry';
	const pathToShutdownRaspberryScript = '/home/pi/Desktop/projects/node-raspberry-rbpMonitoring/server/scripts/script_shutdownRaspberry';

/* pins */
	/*
	let ledConnected = new Gpio(17, 'out'); */
	const gpioPins = [
	/* row 1 */
		new Gpio(17, 'out'),
		new Gpio(27, 'out'),
		new Gpio(22, 'out'),
		new Gpio(5, 'out'),
		new Gpio(6, 'out'),
		new Gpio(13, 'out'),
		new Gpio(19, 'out'),
		new Gpio(26, 'out'),
	/* row 2 */
		new Gpio(18, 'out'),
		new Gpio(23, 'out'),
		new Gpio(24, 'out'),
		new Gpio(25, 'out'),
		new Gpio(12, 'out'),
		new Gpio(16, 'out'),
		new Gpio(20, 'out'),
		new Gpio(21, 'out')
	];

/* stats */
	let rpiStat = {
		countRefresh: 0,
		users: '',
		uptime: '',
		cpuTemp: ''
	}
	let continuouslyCheck;

/* clear path */
	const publicPath = path.join(__dirname, '../public');

/* Port */
	const port = process.env.PORT || 3000;

/* app, server, io */
	let app = express();
	let server = http.createServer(app);
	let io = socketIO(server);


/* serve public folder */
app.use(express.static(publicPath));


/* functions */
/* function to change value of pins */
const useLed = (led, value) => led.writeSync(value);

/* function to clean everything up */
const unexportOnClose = () => {
	gpioPins.forEach(function(currentValue) {
		currentValue.writeSync(0);
		currentValue.unexport();
	});
	console.log('\nswitched all led to off and unexport!');
	console.log('\nbye')
};


/* events */

/* connect event */
io.on('connection', (socket) => {
	console.log('Log: device connected');
	getInfos();

/* send data to client
	let continuouslyCheck = setInterval(function() {
		getInfos;

		if (rpiStat.countRefresh >= lastStat) {
			socket.emit('sendStat', rpiStat);
		}
	}, 60000);
*/
	function sendStat(data) {
		socket.emit('sendStat', data);
	}

/* get raspberrys infos */
	function getInfos() {
		console.log('\nLog: getInfos');

		exec(pathToScript, function(error, stdout, stderr) {
			// countRefresh++;
			console.log('Log Debbug Exec: getInfos');
			if (error != null) console.log('Log Debug exec error: ', error);
			if (stderr) console.log('Log Debug stderr: ', stderr);

			console.log('Log Debug stdout:', stdout);

			/* finding Users */
				let findUsers = /[0-9][\s]users/gi;

				let users = stdout.match(findUsers);
				if (users) {
					console.log('Log Debug Users: ', users[0]);
					rpiStat.users = users[0];

				} else {
					console.log('Log Debug Users: This info is currentlly not available!');
				}

			/* finding Uptime */
				let findUptime = /up[a-z0-9,\s]*minutes|hours|day/gi;
				// let findUptime = /up.:[0-9][0-9][\s],/gi;

				let currentUptime = stdout.match(findUptime);
				if (currentUptime) {
					console.log('Log Debug Uptime: ', currentUptime[0]);
					rpiStat.uptime = currentUptime[0];

				} else {
					console.log('Log Debug Uptime: This info is currentlly not available!');
				}

			/* finding Temp */
				let findCPUTemp = /[0-9][0-9]\.[0-9]'c/gi;

				let currentTemp = stdout.match(findCPUTemp);
				/* watch out currentTemp is an object
				console.log('typeof: ', typeof currentTemp); */
				if (currentTemp) {
					console.log('Log Debug CPU_Temp: ', currentTemp[0]);
					rpiStat.cpuTemp = currentTemp[0];

				} else {
					console.log('Log Debug CPU_Temp: This info is currentlly not available!');
				}

			/* Debug
				console.log('rpiStat', rpiStat);
				console.log('rpiStat Users: ', rpiStat.users);
				console.log('rpiStat Uptime', rpiStat.uptime);
				console.log('rpiStat CPU Temp', rpiStat.cpuTemp); */
				sendStat(rpiStat);
		});
	}

/* Live Data */
	continuouslyCheck = setInterval(getInfos, 15000);

/* Listener for pushedButton */
	socket.on('pushedButton', (button) => {
		console.log('Log: pushedButton', button);

		switch(button.button) {
		/* refresh */
			case 'refresh':
				console.log('Log Debug: ' + button.button + ' not done yet');

			/* send message response to client */
				socket.emit('sendResponsMessage', {
					message: button.button + ': not done yet'
				});
				break;
		/* restart */
			case 'restart':
				console.log('Log Debug: restart nga');

			/* send message response to client */
				socket.emit('countdownMessage', {
					message: 'Raspberry is about to restart in '
				});

			/* exit node app */
				let countdown = 5;
				let countdownInterval = setInterval(function() {
					console.log(countdown);

					countdown--;

					if (countdown === 0) {
						clearInterval(countdownInterval);
						console.log('Raspberry is restarting now');

						exec(pathToRestartRaspberryScript, function(error, stdout, stderr) {
							console.log('Log Debug: restartRaspberry');
							if (error != null) console.log('Log Debug exec error: ', error);
							if (stderr) console.log('Log Debug stderr: ', stderr);

							console.log('Log Debug stdout:', stdout);
						});
					}
				}, 1000);

				break;
		/* exit */
			case 'exit':
				console.log('Log Debug: ' + button.button + ' not done yet');

			/* send message response to client */
				socket.emit('sendResponsMessage', {
					message: button.button + ': not done yet'
				});
				break;
		/* off */
			case 'off':
				console.log('Log Debug: ' + button.button);
				exec(pathToShutdownRaspberryScript, function(error, stdout, stderr) {
					console.log('Log Debug: shutdownRaspberry');
					if (error != null) console.log('Log Debug exec error: ', error);
					if (stderr) console.log('Log Debug stderr: ', stderr);

					console.log('Log Debug stdout:', stdout);

				/* send message response to client */
					socket.emit('sendResponsMessage', {
						message: button.button + ': not done yet'
					});
				});
				break;
		/* default */
			default:
				console.log('Log Debug: what else');

			/* send message response to client */
				socket.emit('sendResponsMessage', {
					message: button.button + ': not done yet'
				});
		}
	});

/* Listener for switchPin */
	socket.on('switchPin', (switchPinData) => {
		console.log('Log: switchPin', switchPinData);
		// console.log(gpioPins);
		let switchThisPin;
		let i;

		/* match the sent pin to the pin variable */
		for (i = 0; i < gpioPins.length; i++) {
			if (switchPinData.pin == gpioPins[i]._gpio) {
				//console.log('bingo !!', gpioPins[i]._gpio);
				switchThisPin = i;

				// to break up for loop
				i = gpioPins.length;
			}
		}

		if (switchPinData.stat === 'On') {
			// useLed(gpioPins[], 1);
			console.log('should switch this pin on');
			console.log('switchThisPin: ', switchThisPin);
			useLed(gpioPins[switchThisPin], 1);

		} else if (switchPinData.stat === 'Off') {
			// useLed(gpioPins[], 0);
			console.log('should switch this pin off');
			console.log('switchThisPin: ', switchThisPin);
			useLed(gpioPins[switchThisPin], 0);
		}

		/* send back res  
		*/
	});

/* disconnect event */
	socket.on('disconnect', () => {
		console.log('device disconnected');
	});
});


/* by pressing CTRL + C it cleans everything up */
	process.on('SIGINT', function () {
		/*
		ledConnected.writeSync(0);
		ledConnected.unexport(); */
		unexportOnClose();
		clearInterval(continuouslyCheck);
		process.exit();
	});

/* server listening on port */
	server.listen(port, () => {
	  console.log(`-- App: Started up at port ${port}`);
	});